import React, { useState, useEffect } from 'react';
import axios from 'axios';


function App() {

const [allCategories, setAllCategories] = useState([])
const [restaurants, setRestaurants] = useState([])
const [modifiedData, setModifiedData] = useState({
  name: '',
  description: '',
  categories: []
})
const [error, setError] = useState(null)


  useEffect(() => {

    // Using an IIFE
      (async function getData() {
          const requestCategories = axios.get("http://localhost:1337/categories")
          const requestRestaurants = axios.get("http://localhost:1337/restaurants")
        try {
          await axios.all([requestCategories, requestRestaurants]).then(axios.spread((...responses) => {

            setAllCategories(responses[0].data)
            setRestaurants(responses[1].data)
          }))
        } catch (error) {
          setError({error})
        }
      })();
  }, [modifiedData])

  const handleInputChange = ({target: {name, value} }) => {

    setModifiedData(prev => {

      return {
        ...prev,
        [name]:value
        }
    })
  };

  const handleSubmit = async e => {
    e.preventDefault();
console.log(modifiedData)
    try {
      const response = await axios.post(
        'http://localhost:1337/restaurants',
        {modifiedData}
        );
      console.log(response);
    } catch (error) {
      setError({ error })
    }
    // clear input field
/*    setModifiedData({
      name: '',
      description: ''
    })*/
  };

  const renderCheckbox = category => {

    const { modifiedData: { categories}} = {modifiedData}
    //const categories = { ...modifiedData }
    //const {modifiedData: { categories }} = this.state;
    const isChecked = categories.includes(category.id);
    const handleChange = () => {
      if (!categories.includes(category.id)) {
        handleInputChange({
          target: { name: 'categories', value: categories.concat(category.id) },
        });
      } else {handleInputChange({
        target: {
          name: 'categories',
          value: categories.filter(v => v !== category.id),
        },
      })
      }
    };

    return (
      <div key={category.id}>
        <label htmlFor={category.id}>{category.name}</label>
        <input
          type="checkbox"
          checked={isChecked}
          onChange={handleChange}
          name='categories'
          id={category.id}
        />
      </div>
      )
  }
/*
  render() {
    const { error, allCategories, modifiedData, restaurants } = this.state;

    // Print errors if any
    if (error) {
      return <div>An error occured: {error.message}</div>;
    }*/

    return (
      <div className="App">
      <form onSubmit={handleSubmit}>
        <h3>Add a restaurant</h3>
        <br />
        <label>
          Name:
            <input
              type="text"
              name="name"
              onChange={handleInputChange}
              value={modifiedData.name}
            />
        </label>
        <label>
            Description:
            <input
              type="text"
              name="description"
              onChange={handleInputChange}
              value={modifiedData.description}
            />
        </label>
        <div>
            <br />
            <b>Select categories</b>
            {allCategories.map(renderCheckbox)}
        </div>
        <br />
        <button type="submit">Submit</button>
        </form>
        {{error}.length &&
          <div>An error occured: {error}</div>
        }
        <ul>
          {restaurants.map(restaurant => (
            <li key={restaurant.id}>
            <h3>{restaurant.name}</h3>
            <p>{restaurant.description}</p>
            </li>

            ))}
        </ul>
      </div>
      );
  }


export default App;
