import React from 'react';
import axios from 'axios';


class App extends React.Component {
constructor(props) {
  super(props)

  this.state = {
    restaurants: [],
    modifiedData: {
      name: '',
      description: '',
      categories: [],
    },
    allCategories: [],
    error: null,
  };
}

  componentDidMount = async () => {

    const requestCategories = axios.get("http://localhost:1337/categories")
    const requestRestaurants = axios.get("http://localhost:1337/restaurants")

    try {
      await axios.all([requestCategories, requestRestaurants]).then(axios.spread((...responses) => {

      this.setState({
        allCategories: responses[0].data,
        restaurants: responses[1].data
      })
      }));
    } catch (error) {
      this.setState({ error });
    }
  };

  componentDidUpdate = async () => {
        const requestCategories = axios.get("http://localhost:1337/categories")
    const requestRestaurants = axios.get("http://localhost:1337/restaurants")

    try {
      await axios.all([requestCategories, requestRestaurants]).then(axios.spread((...responses) => {

      this.setState({
        allCategories: responses[0].data,
        restaurants: responses[1].data
      })
      }));
    } catch (error) {
      this.setState({ error });
    }
  }

  handleInputChange = ({target: {name, value} }) => {
    this.setState(prev => ({
      ...prev,
      modifiedData: {
        ...prev.modifiedData,
        [name]:value,
      },
    }));
  };

  handleSubmit = async e => {
    e.preventDefault();

    try {
      const response = await axios.post(
        'http://localhost:1337/restaurants',
        this.state.modifiedData
        );
      console.log(response);
    } catch (error) {
      this.setState({ error })
    }

    // after it's send to the server clear the input fields
    // works slow when exetuing this code:

/*    this.state.modifiedData.name = "";
    this.state.modifiedData.description = "";*/

  };

  renderCheckbox = category => {
    const {
      modifiedData: { categories },
    } = this.state;
    const isChecked = categories.includes(category.id);
    const handleChange = () => {
      if (!categories.includes(category.id)) {
        this.handleInputChange({
          target: { name: 'categories', value: categories.concat(category.id) },
        });
      } else {this.handleInputChange({
        target: {
          name: 'categories',
          value: categories.filter(v => v !== category.id),
        },
      })
      }
    };

    return (
      <div key={category.id}>
        <label htmlFor={category.id}>{category.name}</label>
        <input
          type="checkbox"
          checked={isChecked}
          onChange={handleChange}
          name='categories'
          id={category.id}
        />
      </div>
      )
  }

  render() {
    const { error, allCategories, modifiedData, restaurants } = this.state;

    // Print errors if any
    if (error) {
      return <div>An error occured: {error.message}</div>;
    }

    return (
      <div className="App">
      <form onSubmit={this.handleSubmit}>
        <h3>Add a restaurant</h3>
        <br />
        <label>
          Name:
            <input
              type="text"
              name="name"
              onChange={this.handleInputChange}
              value={modifiedData.name}
            />
        </label>
        <label>
            Description:
            <input
              type="text"
              name="description"
              onChange={this.handleInputChange}
              value={modifiedData.description}
            />
        </label>
        <div>
            <br />
            <b>Select categories</b>
            {allCategories.map(this.renderCheckbox)}
        </div>
        <br />
        <button type="submit">Submit</button>
        </form>
        <ul>
          {restaurants.map(restaurant => (
            <li key={restaurant.id}>
            <h3>{restaurant.name}</h3>
            <p>{restaurant.description}</p>
            </li>

            ))}
        </ul>
      </div>
      );
  }
}

export default App;
